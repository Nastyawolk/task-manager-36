package ru.t1.volkova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.api.endpoint.IAuthEndpoint;
import ru.t1.volkova.tm.api.service.IAuthService;
import ru.t1.volkova.tm.api.service.IServiceLocator;
import ru.t1.volkova.tm.dto.request.user.UserLoginRequest;
import ru.t1.volkova.tm.dto.request.user.UserLogoutRequest;
import ru.t1.volkova.tm.dto.request.user.UserProfileRequest;
import ru.t1.volkova.tm.dto.response.user.UserLoginResponse;
import ru.t1.volkova.tm.dto.response.user.UserLogoutResponse;
import ru.t1.volkova.tm.dto.response.user.UserProfileResponse;
import ru.t1.volkova.tm.model.Session;
import ru.t1.volkova.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.volkova.tm.api.endpoint.IAuthEndpoint")
public final class AuthEndpoint extends AbstractEndPoint implements IAuthEndpoint {

    public AuthEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    @WebMethod
    public UserLoginResponse login(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLoginRequest request
    ) {
        @NotNull final IAuthService authService = getServiceLocator().getAuthService();
        @NotNull final String token = authService.login(request.getLogin(), request.getPassword());
        return new UserLoginResponse(token);
    }

    @Override
    @NotNull
    @WebMethod
    public UserLogoutResponse logout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLogoutRequest request
    ) {
        final Session session = check(request);
        getServiceLocator().getAuthService().invalidate(session);
        return new UserLogoutResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserProfileResponse profile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserProfileRequest request
    ) {
        @Nullable Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final User user = getServiceLocator().getUserService().findOneById(userId);
        return new UserProfileResponse(user);
    }

}
