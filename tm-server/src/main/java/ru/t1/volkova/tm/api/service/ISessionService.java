package ru.t1.volkova.tm.api.service;

import ru.t1.volkova.tm.model.Session;
import ru.t1.volkova.tm.model.Task;

public interface ISessionService extends IUserOwnedService<Session> {
}
