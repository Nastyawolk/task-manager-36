package ru.t1.volkova.tm.repository;

import ru.t1.volkova.tm.api.repository.IProjectRepository;
import ru.t1.volkova.tm.api.repository.ISessionRepository;
import ru.t1.volkova.tm.model.Project;
import ru.t1.volkova.tm.model.Session;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

}
