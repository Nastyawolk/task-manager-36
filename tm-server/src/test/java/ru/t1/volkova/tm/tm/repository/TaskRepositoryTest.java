package ru.t1.volkova.tm.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.volkova.tm.api.repository.IProjectRepository;
import ru.t1.volkova.tm.api.repository.ITaskRepository;
import ru.t1.volkova.tm.enumerated.TaskSort;
import ru.t1.volkova.tm.model.Project;
import ru.t1.volkova.tm.model.Task;
import ru.t1.volkova.tm.repository.ProjectRepository;
import ru.t1.volkova.tm.repository.TaskRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class TaskRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 6;

    private static final String USER_ID_1 = UUID.randomUUID().toString();

    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private List<Task> taskList;

    @NotNull
    private ITaskRepository taskRepository;

    @NotNull
    private IProjectRepository projectRepository;

    @Before
    public void initRepository() {
        taskList = new ArrayList<>();
        taskRepository = new TaskRepository();
        projectRepository = new ProjectRepository();
        @NotNull Project project = new Project();
        @NotNull Project project2 = new Project();
        projectRepository.add(project);
        projectRepository.add(project2);
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull Task task = new Task();
            task.setName("task" + i);
            task.setDescription("Tdescription" + i);
            if (i <= 3)
            {
                task.setUserId(USER_ID_1);
                task.setProjectId(project.getId());
            }
            else
            {
                task.setUserId(USER_ID_2);
                task.setProjectId(project2.getId());
            }
            taskRepository.add(task);
            taskList.add(task);
        }
    }

    @Test
    public void testAddTask() {
        Integer expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final Task newTask = new Task();
        taskRepository.add(newTask);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testAddForUserId() {
        Integer expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final String name = "Test task";
        @NotNull final String description = "Test description";
        @NotNull Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(USER_ID_1, task);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
        @Nullable final Task actualTask = taskRepository.findOneById(USER_ID_1, task.getId());
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(name, actualTask.getName());
        Assert.assertEquals(description, actualTask.getDescription());
        Assert.assertEquals(USER_ID_1, actualTask.getUserId());
    }

    @Test
    public void testAddAll() {
        final int recordsCount = NUMBER_OF_ENTRIES / 2;
        final Integer expectedNumberOfEntries = NUMBER_OF_ENTRIES + recordsCount;
        @NotNull final List<Task> taskList = new ArrayList<>();
        for (int i = 0; i < recordsCount; i++) {
            taskList.add(new Task());
        }
        taskRepository.add(taskList);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testSet() {
        final int recordsCount = NUMBER_OF_ENTRIES / 2;
        final Integer expectedNumberOfEntries = recordsCount;
        final int oldTaskSize = taskRepository.findAll().size();
        @NotNull final List<Task> taskList = new ArrayList<>();
        for (int i = 0; i < recordsCount; i++) {
            taskList.add(new Task());
        }
        taskRepository.set(taskList);
        final int newTaskSize = taskRepository.findAll().size();
        Assert.assertNotEquals(newTaskSize, oldTaskSize);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Task> taskList = taskRepository.findAll();
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskList.size());
    }

    @Test
    public void testFindAllWithComparator() {
        @NotNull final String sortType = "BY_NAME";
        @Nullable final TaskSort sort = TaskSort.toSort(sortType);
        if (sort != null) {
            @NotNull final List<Task> taskList = taskRepository.findAll(sort.getComparator());
            Assert.assertEquals(sortType, TaskSort.BY_NAME.toString());
            Assert.assertEquals(NUMBER_OF_ENTRIES, taskList.size());
        }
    }

    @Test
    public void testFindAllForUser() {
        @Nullable final List<Task> taskList = taskRepository.findAll(USER_ID_1);
        @Nullable final List<Task> taskList2 = taskRepository.findAll(USER_ID_2);
        if (taskList != null && taskList2 != null) {
            Assert.assertEquals(NUMBER_OF_ENTRIES / 2 + 1, taskList.size());
            Assert.assertEquals(NUMBER_OF_ENTRIES / 2 - 1, taskList2.size());
        }
    }

    @Test
    public void testFindAllForUserNegative() {
        @Nullable final List<Task> taskList = taskRepository.findAll((String) null);
        Assert.assertNull(taskList);
        @Nullable final List<Task> taskList2 = taskRepository.findAll("non-existent-id");
        if (taskList2 == null) return;
        Assert.assertEquals(0, taskList2.size());
    }

    @Test
    public void testFindAllWithComparatorForUser() {
        @NotNull final String sortType = "BY_STATUS";
        @Nullable final TaskSort sort = TaskSort.toSort(sortType);
        if (sort != null) {
            @Nullable final List<Task> taskList = taskRepository.findAll(USER_ID_1, sort.getComparator());
            Assert.assertEquals(sortType, TaskSort.BY_STATUS.toString());
            if (taskList == null) return;
            Assert.assertEquals(NUMBER_OF_ENTRIES / 2 + 1, taskList.size());
        }
    }

    @Test
    public void testFindAllWithComparatorForUserNegative() {
        @NotNull final String sortType = "BY_STATUS";
        @Nullable final TaskSort sort = TaskSort.toSort(sortType);
        if (sort != null) {
            @Nullable final List<Task> taskList = taskRepository.findAll("non-existent-id", sort.getComparator());
            Assert.assertNotEquals(sortType, TaskSort.BY_NAME.toString());
            if (taskList == null) return;
            Assert.assertEquals(0, taskList.size());
        }
    }

    @Test
    public void testFindOneById() {
        @Nullable final String taskId1 = taskRepository.findOneByIndex(1).getId();
        @Nullable final String taskId2 = taskRepository.findOneByIndex(2).getId();
        @NotNull final Task expected1 = taskList.get(1);
        @NotNull final Task expected2 = taskList.get(2);
        Assert.assertEquals(expected1, taskRepository.findOneById(taskId1));
        Assert.assertEquals(expected2, taskRepository.findOneById(taskId2));
    }

    @Test
    public void testFindOneByIdNegative() {
        Assert.assertNull(taskRepository.findOneById("NotExcitingId"));
    }

    @Test
    public void testFindOneByIdForUser() {
        for (int i = 0; i < NUMBER_OF_ENTRIES / 2 + 1; i++) {
            @NotNull final String id = taskList.get(i).getId();
            Assert.assertEquals(taskList.get(i), taskRepository.findOneById(USER_ID_1, id));
        }
        for (int i = NUMBER_OF_ENTRIES / 2 + 1; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final String id = taskList.get(i).getId();
            Assert.assertEquals(taskList.get(i), taskRepository.findOneById(USER_ID_2, id));
        }
    }

    @Test
    public void testFindOneByIdForUserNegative() {
        Assert.assertNull(taskRepository.findOneById(null, taskList.get(1).getId()));
        Assert.assertNull(taskRepository.findOneById(USER_ID_1, "NotExcitingId"));
        Assert.assertNull(taskRepository.findOneById(USER_ID_2, "NotExcitingId"));
    }

    @Test
    public void testFindOneByIndex() {
        @Nullable final Task task1 = taskRepository.findOneByIndex(1);
        @Nullable final Task task2 = taskRepository.findOneByIndex(2);
        @NotNull final Task expected1 = taskList.get(1);
        @NotNull final Task expected2 = taskList.get(2);
        Assert.assertEquals(expected1, task1);
        Assert.assertEquals(expected2, task2);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testFindOneByIndexNegative() {
        Assert.assertNotNull(taskRepository.findOneByIndex(NUMBER_OF_ENTRIES + 20));
        Assert.assertNotNull(taskRepository.findOneByIndex(NUMBER_OF_ENTRIES * 2));
    }

    @Test
    public void testFindOneByIndexForUser() {
        @Nullable final Task task1 = taskRepository.findOneByIndex(1);
        @Nullable final Task task2 = taskRepository.findOneByIndex(2);
        @NotNull final Task expected1 = taskList.get(1);
        @NotNull final Task expected2 = taskList.get(2);
        Assert.assertEquals(expected1, task1);
        Assert.assertEquals(expected2, task2);

    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testFindOneByIndexForUserNegative() {
        Assert.assertNotNull(taskRepository.findOneByIndex(NUMBER_OF_ENTRIES+20));
        Assert.assertNotNull(taskRepository.findOneByIndex(NUMBER_OF_ENTRIES*2));
    }

    @Test
    public void testRemoveOne() {
        @Nullable final Task task1 = taskRepository.findOneByIndex(3);
        @Nullable final Task task2 = taskRepository.findOneByIndex(5);
        Assert.assertEquals(taskList.get(3), taskRepository.removeOne(task1));
        Assert.assertEquals(taskList.get(5), taskRepository.removeOne(task2));
    }

    @Test
    public void testRemoveOneNegative() {
        @NotNull final Task task1 = new Task();
        @NotNull final Task task2 = new Task();
        Assert.assertNull(taskRepository.removeOne(task1));
        Assert.assertNull(taskRepository.removeOne(task2));
    }

    @Test
    public void testRemoveOneForUser() {
        @Nullable final Task task1 = taskRepository.findOneByIndex(3);
        @Nullable final Task task2 = taskRepository.findOneByIndex(5);
        Assert.assertEquals(taskList.get(3), taskRepository.removeOne(USER_ID_1, task1));
        Assert.assertEquals(taskList.get(5), taskRepository.removeOne(USER_ID_2, task2));
    }

    @Test
    public void testRemoveOneForUserNegative() {
        @NotNull final Task task1 = new Task();
        @NotNull final Task task2 = new Task();
        Assert.assertNull(taskRepository.removeOne(USER_ID_1, task1));
        Assert.assertNull(taskRepository.removeOne(USER_ID_2, task2));
    }

    @Test
    public void testRemoveOneById() {
        @Nullable final String taskId1 = taskRepository.findOneByIndex(4).getId();
        @Nullable final String taskId2 = taskRepository.findOneByIndex(3).getId();
        @NotNull final Task expected1 = taskList.get(4);
        @NotNull final Task expected2 = taskList.get(3);
        Assert.assertEquals(expected1, taskRepository.removeOneById(taskId1));
        Assert.assertEquals(expected2, taskRepository.removeOneById(taskId2));

        Assert.assertNotEquals(expected2, taskRepository.removeOneById(taskId1));
        Assert.assertNotEquals(expected1, taskRepository.removeOneById(taskId2));
    }

    @Test
    public void testRemoveOneByIdNegative() {
        @NotNull final String taskId1 = UUID.randomUUID().toString();
        @NotNull final String taskId2 = UUID.randomUUID().toString();
        Assert.assertNull(taskRepository.removeOneById(taskId1));
        Assert.assertNull(taskRepository.removeOneById(taskId2));
    }

    @Test
    public void testRemoveOneByIdForUser() {
        for (int i = 0; i < NUMBER_OF_ENTRIES / 2 + 1; i++) {
            @NotNull final String id = taskList.get(i).getId();
            Assert.assertEquals(taskList.get(i), taskRepository.removeOneById(USER_ID_1, id));
        }
        for (int i = NUMBER_OF_ENTRIES / 2 + 1; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final String id = taskList.get(i).getId();
            Assert.assertEquals(taskList.get(i), taskRepository.removeOneById(USER_ID_2, id));
        }
    }

    @Test
    public void testRemoveOneByIdForUserNegative() {
        @NotNull final String taskId1 = UUID.randomUUID().toString();
        @NotNull final String taskId2 = UUID.randomUUID().toString();
        Assert.assertNull(taskRepository.removeOneById(USER_ID_1, taskId1));
        Assert.assertNull(taskRepository.removeOneById(USER_ID_2, taskId2));
        Assert.assertNull(taskRepository.removeOneById("NotExcitingId", taskId1));
        Assert.assertNull(taskRepository.removeOneById(null, taskId2));
    }

    @Test
    public void testRemoveAll() {
        taskRepository.removeAll();
        Assert.assertEquals(0, taskRepository.getSize().intValue());
    }

    @Test
    public void testRemoveAllForUser() {
        taskRepository.removeAll(USER_ID_1);
        taskRepository.removeAll(USER_ID_2);
        Assert.assertEquals(0, taskRepository.getSize(USER_ID_1));
        Assert.assertEquals(0, taskRepository.getSize(USER_ID_2));
    }

    @Test
    public void testRemoveAllForUserNegative() {
        taskRepository.removeAll("NotExcitingId");
        taskRepository.removeAll((String) null);
        Assert.assertNotEquals(0, taskRepository.getSize(USER_ID_1));
        Assert.assertNotEquals(0, taskRepository.getSize(USER_ID_2));
    }

    @Test
    public void testRemoveOneByIndex() {
        @Nullable final Task task1 = taskRepository.removeOneByIndex(1);
        Assert.assertEquals(NUMBER_OF_ENTRIES-1, taskRepository.getSize().intValue());
        @Nullable final Task task2 = taskRepository.removeOneByIndex(4);
        Assert.assertEquals(NUMBER_OF_ENTRIES-2, taskRepository.getSize().intValue());

        if (task1 != null && task2 != null) {
            Assert.assertNull(taskRepository.findOneById(task1.getId()));
            Assert.assertNull(taskRepository.findOneById(task2.getId()));
        }
    }

    @Test
    public void testRemoveOneByIndexNegative() {
        Assert.assertNull(taskRepository.removeOneByIndex(null));
    }

    @Test
    public void testRemoveOneByIndexForUser() {
        @Nullable final Task task1 = taskRepository.removeOneByIndex(USER_ID_1, 1);
        Assert.assertEquals(NUMBER_OF_ENTRIES-1, taskRepository.getSize().intValue());
        @Nullable final Task task2 = taskRepository.removeOneByIndex(USER_ID_2, 2);
        Assert.assertEquals(NUMBER_OF_ENTRIES-1, taskRepository.getSize().intValue());

        if (task1 != null && task2 != null) {
            Assert.assertNull(taskRepository.findOneById(task1.getId()));
            Assert.assertNull(taskRepository.findOneById(task2.getId()));
        }
    }

    @Test
    public void testRemoveOneByIndexForUserNegative() {
        Assert.assertNull(taskRepository.removeOneByIndex(USER_ID_1, 77));
        Assert.assertNull(taskRepository.removeOneByIndex(USER_ID_2, 20));
        Assert.assertNull(taskRepository.removeOneByIndex("NotExcitingId", 2));
        Assert.assertNull(taskRepository.removeOneByIndex(null, 3));
    }


    @Test
    public void testGetSize() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskRepository.getSize().intValue());
        Assert.assertEquals(taskList.size(), taskRepository.getSize().intValue());
    }

    @Test
    public void testGetSizeForUser() {
        Assert.assertEquals(NUMBER_OF_ENTRIES/2+1, taskRepository.getSize(USER_ID_1));
        Assert.assertEquals(NUMBER_OF_ENTRIES/2-1, taskRepository.getSize(USER_ID_2));
        Assert.assertEquals(taskList.size()/2+1, taskRepository.getSize(USER_ID_1));
        Assert.assertEquals(taskList.size()/2-1, taskRepository.getSize(USER_ID_2));
    }


    @Test
    public void testExistsById() {
        for (int i = 0; i < NUMBER_OF_ENTRIES/2+1; i++) {
            @NotNull final String id = taskList.get(i).getId();
            Assert.assertTrue(taskRepository.existsById(USER_ID_1, id));
        }
        for (int i = NUMBER_OF_ENTRIES/2+1; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final String id = taskList.get(i).getId();
            Assert.assertTrue(taskRepository.existsById(USER_ID_2, id));
        }
    }

    @Test
    public void testExistsByIdNegative() {
        @NotNull final String userId1 = UUID.randomUUID().toString();
        @NotNull final String userId2 = UUID.randomUUID().toString();
        for (int i = 0; i < NUMBER_OF_ENTRIES/2+1; i++) {
            @NotNull final String id = taskList.get(i).getId();
            Assert.assertFalse(taskRepository.existsById(userId1, id));
        }
        for (int i = NUMBER_OF_ENTRIES/2+1; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final String id = taskList.get(i).getId();
            Assert.assertFalse(taskRepository.existsById(userId2, id));
        }
    }

    @Test
    public void findAllByProjectId() {
        @Nullable final List<Task> tasks = taskRepository.findAllByProjectId(USER_ID_1, projectRepository.findOneByIndex(0).getId());
        @Nullable final List<Task> tasks2 = taskRepository.findAllByProjectId(USER_ID_2, projectRepository.findOneByIndex(1).getId());
        Assert.assertNotNull(tasks);
        Assert.assertNotNull(tasks2);
    }

    @Test
    public void findAllByProjectIdNegative() {
        @Nullable final List<Task> tasks = taskRepository.findAllByProjectId(null, projectRepository.findOneByIndex(0).getId());
        @Nullable final Project project = new Project();
        @Nullable final List<Task> tasks2 = taskRepository.findAllByProjectId(USER_ID_2, project.getId());
        Assert.assertNull(tasks);
        if (tasks2 == null) return;
        Assert.assertEquals(0, tasks2.size());
    }

}
