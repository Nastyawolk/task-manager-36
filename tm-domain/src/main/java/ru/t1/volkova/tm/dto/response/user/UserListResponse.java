package ru.t1.volkova.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.dto.response.AbstractResponse;
import ru.t1.volkova.tm.model.Task;
import ru.t1.volkova.tm.model.User;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class UserListResponse extends AbstractResponse {

    @Nullable
    private List<User> users;

    public UserListResponse(@Nullable List<User> users) {
        this.users = users;
    }

}
