package ru.t1.volkova.tm.dto.request.data;

import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.dto.request.AbstractUserRequest;

public final class DataBase64SaveRequest extends AbstractUserRequest {

    public DataBase64SaveRequest(@Nullable final String token) {
        super(token);
    }

}
