package ru.t1.volkova.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.volkova.tm.api.endpoint.IAuthEndpoint;
import ru.t1.volkova.tm.api.endpoint.IProjectEndpoint;
import ru.t1.volkova.tm.dto.request.project.*;
import ru.t1.volkova.tm.dto.request.user.UserLoginRequest;
import ru.t1.volkova.tm.dto.response.project.*;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.marker.SoapCategory;
import ru.t1.volkova.tm.model.Project;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ProjectEndpointTest {

    private static final int NUMBER_OF_ENTRIES = 5;

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstanse();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstanse();

    @Nullable
    private String token;

    @NotNull
    private List<Project> projectList;

    private int projectSize;

    @Before
    public void initTest() {
        @NotNull final UserLoginRequest requestLogin = new UserLoginRequest();
        requestLogin.setLogin("test");
        requestLogin.setPassword("test");
        token = authEndpoint.login(requestLogin).getToken();

        projectList = new ArrayList<>();

        @NotNull final ProjectListRequest requestList = new ProjectListRequest(token);
        @NotNull final ProjectListResponse responseList = projectEndpoint.listProject(requestList);
        if (responseList.getProjects() != null) {
            projectList.addAll(responseList.getProjects());
        }

        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final ProjectCreateRequest requestCreate = new ProjectCreateRequest(token);
            requestCreate.setName("Cproject" + i);
            requestCreate.setDescription("description" + i);
            @NotNull final ProjectCreateResponse responseCreate = projectEndpoint.createProject(requestCreate);
            projectList.add(responseCreate.getProject());
        }
        projectSize = projectList.size();
    }

    @Test
    @Category(SoapCategory.class)
    public void testChangeProjectStatusById() {
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(token);
        @NotNull final Random random = new Random();
        @NotNull final String id = projectList.get(random.nextInt(projectSize)).getId();
        @NotNull final String statusValue = "COMPLETED";
        @Nullable final Status status = Status.toStatus(statusValue);
        request.setId(id);
        request.setStatus(status);
        @NotNull final ProjectChangeStatusByIdResponse response = projectEndpoint.changeProjectStatusById(request);
        if (response.getProject() == null) return;
        Assert.assertEquals(status, response.getProject().getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void testChangeProjectStatusByIndex() {
        @NotNull final ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest(token);
        @NotNull final Random random = new Random();
        @NotNull final Project project = projectList.get(random.nextInt(projectSize));
        @NotNull final Integer index = projectList.indexOf(project);
        @NotNull final String statusValue = "COMPLETED";
        @Nullable final Status status = Status.toStatus(statusValue);
        request.setIndex(index);
        request.setStatus(status);
        @NotNull final ProjectChangeStatusByIndexResponse response = projectEndpoint.changeProjectStatusByIndex(request);
        if (response.getProject() == null) return;
        Assert.assertEquals(status, response.getProject().getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void testClearProjects() {
        ProjectClearResponse response = projectEndpoint.clearProject(new ProjectClearRequest(token));
        Assert.assertNull(response.getProjects());
    }

    @Test
    @Category(SoapCategory.class)
    public void testCompleteProjectById() {
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(token);
        @NotNull final Random random = new Random();
        @NotNull final String id = projectList.get(random.nextInt(projectSize)).getId();
        @NotNull final String statusValue = "COMPLETED";
        @Nullable final Status status = Status.toStatus(statusValue);
        request.setId(id);
        request.setStatus(status);
        @NotNull final ProjectChangeStatusByIdResponse response = projectEndpoint.changeProjectStatusById(request);
        if (response.getProject() == null) return;
        Assert.assertEquals(status, response.getProject().getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void testCompleteProjectByIndex() {
        @NotNull final ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest(token);
        @NotNull final Random random = new Random();
        @NotNull final Project project = projectList.get(random.nextInt(projectSize));
        @NotNull final Integer index = projectList.indexOf(project);
        @NotNull final String statusValue = "COMPLETED";
        @Nullable final Status status = Status.toStatus(statusValue);
        request.setIndex(index);
        request.setStatus(status);
        @NotNull final ProjectChangeStatusByIndexResponse response = projectEndpoint.changeProjectStatusByIndex(request);
        if (response.getProject() == null) return;
        Assert.assertEquals(status, response.getProject().getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void testCreateProject() {
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(token);
        @NotNull final String projectName = "new project";
        @NotNull final String description = "new description";
        request.setName(projectName);
        request.setDescription(description);
        @NotNull final ProjectCreateResponse response = projectEndpoint.createProject(request);
        Assert.assertNotNull(response);
        if (response.getProject() == null) return;
        Assert.assertEquals(projectName, response.getProject().getName());
        Assert.assertEquals(description, response.getProject().getDescription());
    }

    @Test
    @Category(SoapCategory.class)
    public void testListProject() {
        @NotNull final ProjectListRequest requestList = new ProjectListRequest(token);
        @NotNull final ProjectListResponse responseList = projectEndpoint.listProject(requestList);
        if (responseList.getProjects() == null) return;
        Assert.assertEquals(projectSize, responseList.getProjects().size());
    }

    @Test
    @Category(SoapCategory.class)
    public void testRemoveProjectById() {
        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(token);
        @NotNull final Random random = new Random();
        @NotNull final String id = projectList.get(random.nextInt(projectSize)).getId();
        request.setId(id);
        projectEndpoint.removeProjectById(request);
        @NotNull final ProjectListRequest requestList = new ProjectListRequest(token);
        @NotNull final ProjectListResponse responseList = projectEndpoint.listProject(requestList);
        if (responseList.getProjects() == null) return;
        Assert.assertNotEquals(projectSize, responseList.getProjects().size());
    }

    @Test
    @Category(SoapCategory.class)
    public void testRemoveProjectByIndex() {
        @NotNull final ProjectRemoveByIndexRequest request = new ProjectRemoveByIndexRequest(token);
        @NotNull final Random random = new Random();
        @NotNull final Project project = projectList.get(random.nextInt(projectSize));
        @NotNull final Integer index = projectList.indexOf(project);
        request.setIndex(index);
        projectEndpoint.removeProjectByIndex(request);
        @NotNull final ProjectListRequest requestList = new ProjectListRequest(token);
        @NotNull final ProjectListResponse responseList = projectEndpoint.listProject(requestList);
        if (responseList.getProjects() == null) return;
        Assert.assertNotEquals(projectSize, responseList.getProjects().size());
    }

    @Test
    @Category(SoapCategory.class)
    public void testGetProjectById() {
        @NotNull final ProjectGetByIdRequest request = new ProjectGetByIdRequest(token);
        @NotNull final Random random = new Random();
        @NotNull final String id = projectList.get(random.nextInt(projectSize)).getId();
        request.setId(id);
        @NotNull final ProjectGetByIdResponse response = projectEndpoint.getProjectById(request);
        if (response.getProject() == null) return;
        Assert.assertEquals(id, response.getProject().getId());
    }

    @Test
    @Category(SoapCategory.class)
    public void testGetProjectByIndex() {
        @NotNull final ProjectGetByIndexRequest request = new ProjectGetByIndexRequest(token);
        @NotNull final ProjectListResponse responseList = projectEndpoint.listProject(new ProjectListRequest(token));
        @NotNull final Random random = new Random();
        if (responseList.getProjects() == null) return;
        @NotNull final Project project = responseList.getProjects().get(random.nextInt(projectSize));
        final int index = responseList.getProjects().indexOf(project);
        request.setIndex(index);
        @NotNull final ProjectGetByIndexResponse response = projectEndpoint.getProjectByIndex(request);
        if (response.getProject() == null) return;
        Assert.assertNotNull(response.getProject());
    }

    @Test
    @Category(SoapCategory.class)
    public void testStartProjectById() {
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(token);
        @NotNull final Random random = new Random();
        @NotNull final String id = projectList.get(random.nextInt(projectSize)).getId();
        @NotNull final String statusValue = "IN_PROGRESS";
        @Nullable final Status status = Status.toStatus(statusValue);
        request.setId(id);
        request.setStatus(status);
        @NotNull final ProjectChangeStatusByIdResponse response = projectEndpoint.changeProjectStatusById(request);
        if (response.getProject() == null) return;
        Assert.assertEquals(status, response.getProject().getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void testStartProjectByIndex() {
        @NotNull final ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest(token);
        @NotNull final Random random = new Random();
        @NotNull final Project project = projectList.get(random.nextInt(projectSize));
        @NotNull final Integer index = projectList.indexOf(project);
        @NotNull final String statusValue = "IN_PROGRESS";
        @Nullable final Status status = Status.toStatus(statusValue);
        request.setIndex(index);
        request.setStatus(status);
        @NotNull final ProjectChangeStatusByIndexResponse response = projectEndpoint.changeProjectStatusByIndex(request);
        if (response.getProject() == null) return;
        Assert.assertEquals(status, response.getProject().getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void testUpdateProjectById() {
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(token);
        @NotNull final Random random = new Random();
        @NotNull final String id = projectList.get(random.nextInt(projectSize)).getId();
        @NotNull final String name = "new_name";
        @NotNull final String description = "new description";
        request.setId(id);
        request.setName(name);
        request.setDescription(description);
        @Nullable final Project project = projectEndpoint.updateProjectById(request).getProject();
        if (project == null) {return;}
        Assert.assertEquals(name, project.getName());
        Assert.assertEquals(description, project.getDescription());
    }

    @Test
    @Category(SoapCategory.class)
    public void testUpdateProjectByIndex() {
        @NotNull final ProjectUpdateByIndexRequest request = new ProjectUpdateByIndexRequest(token);
        @NotNull final Random random = new Random();
        @NotNull final Project project = projectList.get(random.nextInt(projectSize));
        @NotNull final Integer index = projectList.indexOf(project);
        @NotNull final String name = "new_name";
        @NotNull final String description = "new description";
        request.setIndex(index);
        request.setName(name);
        request.setDescription(description);
        @Nullable final Project projectUpdated = projectEndpoint.updateProjectByIndex(request).getProject();
        if (projectUpdated == null) {return;}
        Assert.assertEquals(name, projectUpdated.getName());
        Assert.assertEquals(description, projectUpdated.getDescription());
    }

}
