package ru.t1.volkova.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.volkova.tm.api.endpoint.IAuthEndpoint;
import ru.t1.volkova.tm.api.endpoint.IProjectEndpoint;
import ru.t1.volkova.tm.api.endpoint.ITaskEndpoint;
import ru.t1.volkova.tm.dto.request.project.ProjectListRequest;
import ru.t1.volkova.tm.dto.request.task.*;
import ru.t1.volkova.tm.dto.request.user.UserLoginRequest;
import ru.t1.volkova.tm.dto.response.project.ProjectListResponse;
import ru.t1.volkova.tm.dto.response.task.*;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.marker.SoapCategory;
import ru.t1.volkova.tm.model.Project;
import ru.t1.volkova.tm.model.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class TaskEndpointTest {

    private static final int NUMBER_OF_ENTRIES = 5;

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstanse();

    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstanse();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstanse();

    @Nullable
    private String token;

    @NotNull
    private List<Task> taskList;

    @NotNull
    private List<Project> projectList;

    private int taskSize;

    private int projectSize;

    @Before
    public void initTest() {
        @NotNull final UserLoginRequest requestLogin = new UserLoginRequest();
        requestLogin.setLogin("test");
        requestLogin.setPassword("test");
        token = authEndpoint.login(requestLogin).getToken();

        taskList = new ArrayList<>();

        @NotNull final TaskListRequest requestList = new TaskListRequest(token);
        @NotNull final TaskListResponse responseList = taskEndpoint.listTask(requestList);
        if (responseList.getTasks() != null) {
            taskList.addAll(responseList.getTasks());
        }

        projectList = new ArrayList<>();

        @NotNull final ProjectListRequest requestProjectList = new ProjectListRequest(token);
        @NotNull final ProjectListResponse responseProjectList = projectEndpoint.listProject(requestProjectList);
        if (responseProjectList.getProjects() != null) {
            projectList.addAll(responseProjectList.getProjects());
        }

        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final TaskCreateRequest requestCreate = new TaskCreateRequest(token);
            requestCreate.setName("Ctask" + i);
            requestCreate.setDescription("description" + i);
            @NotNull final TaskCreateResponse responseCreate = taskEndpoint.createTask(requestCreate);
            taskList.add(responseCreate.getTask());
        }
        taskSize = taskList.size();
        projectSize = projectList.size();
    }

    @Test
    @Category(SoapCategory.class)
    public void testBindTaskToProject() {
        @NotNull final Random random = new Random();
        @NotNull final String projectID = projectList.get(random.nextInt(projectSize)).getId();
        @NotNull final String taskID = taskList.get(random.nextInt(taskSize)).getId();
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(token);
        request.setProjectId(projectID);
        request.setTaskId(taskID);
        taskEndpoint.bindTaskToProject(request);

        @NotNull final TaskListByProjectIdRequest requestList = new TaskListByProjectIdRequest(token);
        requestList.setProjectId(projectID);
        @Nullable final List<Task> tasks = taskEndpoint.getTaskByProjectId(requestList).getTasks();
        if (tasks == null) {return;}
        Assert.assertNotEquals(tasks.size(), 0);
    }

    @Test
    @Category(SoapCategory.class)
    public void testChangeTaskStatusById() {
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(token);
        @NotNull final Random random = new Random();
        @NotNull final String id = taskList.get(random.nextInt(taskSize)).getId();
        @NotNull final String statusValue = "COMPLETED";
        @Nullable final Status status = Status.toStatus(statusValue);
        request.setId(id);
        request.setStatus(status);
        @NotNull final TaskChangeStatusByIdResponse response = taskEndpoint.changeTaskStatusById(request);
        if (response.getTask() == null) return;
        Assert.assertEquals(status, response.getTask().getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void testChangeTaskStatusByIndex() {
        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest(token);
        @NotNull final Random random = new Random();
        @NotNull final Task task = taskList.get(random.nextInt(taskSize));
        @NotNull final Integer index = taskList.indexOf(task);
        @NotNull final String statusValue = "COMPLETED";
        @Nullable final Status status = Status.toStatus(statusValue);
        request.setIndex(index);
        request.setStatus(status);
        @NotNull final TaskChangeStatusByIndexResponse response = taskEndpoint.changeTaskStatusByIndex(request);
        if (response.getTask() == null) return;
        Assert.assertEquals(status, response.getTask().getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void testClearTasks() {
        TaskClearResponse response = taskEndpoint.clearTask(new TaskClearRequest(token));
        Assert.assertNull(response.getTasks());
    }

    @Test
    @Category(SoapCategory.class)
    public void testCompleteTaskById() {
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(token);
        @NotNull final Random random = new Random();
        @NotNull final String id = taskList.get(random.nextInt(taskSize)).getId();
        @NotNull final String statusValue = "COMPLETED";
        @Nullable final Status status = Status.toStatus(statusValue);
        request.setId(id);
        request.setStatus(status);
        @NotNull final TaskChangeStatusByIdResponse response = taskEndpoint.changeTaskStatusById(request);
        if (response.getTask() == null) return;
        Assert.assertEquals(status, response.getTask().getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void testCompleteTaskByIndex() {
        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest(token);
        @NotNull final Random random = new Random();
        @NotNull final Task task = taskList.get(random.nextInt(taskSize));
        @NotNull final Integer index = taskList.indexOf(task);
        @NotNull final String statusValue = "COMPLETED";
        @Nullable final Status status = Status.toStatus(statusValue);
        request.setIndex(index);
        request.setStatus(status);
        @NotNull final TaskChangeStatusByIndexResponse response = taskEndpoint.changeTaskStatusByIndex(request);
        if (response.getTask() == null) return;
        Assert.assertEquals(status, response.getTask().getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void testCreateTask() {
        @NotNull final TaskCreateRequest request = new TaskCreateRequest(token);
        @NotNull final String taskName = "new task";
        @NotNull final String description = "new description";
        request.setName(taskName);
        request.setDescription(description);
        @NotNull final TaskCreateResponse response = taskEndpoint.createTask(request);
        Assert.assertNotNull(response);
        if (response.getTask() == null) return;
        Assert.assertEquals(taskName, response.getTask().getName());
        Assert.assertEquals(description, response.getTask().getDescription());
    }

    @Test
    @Category(SoapCategory.class)
    public void testListTask() {
        @NotNull final TaskListRequest requestList = new TaskListRequest(token);
        @NotNull final TaskListResponse responseList = taskEndpoint.listTask(requestList);
        if (responseList.getTasks() == null) return;
        Assert.assertEquals(taskSize, responseList.getTasks().size());
    }

    @Test
    @Category(SoapCategory.class)
    public void testRemoveTaskById() {
        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(token);
        @NotNull final Random random = new Random();
        @NotNull final String id = taskList.get(random.nextInt(taskSize)).getId();
        request.setId(id);
        taskEndpoint.removeTaskById(request);
        @NotNull final TaskListRequest requestList = new TaskListRequest(token);
        @NotNull final TaskListResponse responseList = taskEndpoint.listTask(requestList);
        if (responseList.getTasks() == null) return;
        Assert.assertNotEquals(taskSize, responseList.getTasks().size());
    }

    @Test
    @Category(SoapCategory.class)
    public void testRemoveTaskByIndex() {
        @NotNull final TaskRemoveByIndexRequest request = new TaskRemoveByIndexRequest(token);
        @NotNull final Random random = new Random();
        @NotNull final Task task = taskList.get(random.nextInt(taskSize));
        @NotNull final Integer index = taskList.indexOf(task);
        request.setIndex(index);
        taskEndpoint.removeTaskByIndex(request);
        @NotNull final TaskListRequest requestList = new TaskListRequest(token);
        @NotNull final TaskListResponse responseList = taskEndpoint.listTask(requestList);
        if (responseList.getTasks() == null) return;
        Assert.assertNotEquals(taskSize, responseList.getTasks().size());
    }

    @Test
    @Category(SoapCategory.class)
    public void testGetTaskById() {
        @NotNull final TaskGetByIdRequest request = new TaskGetByIdRequest(token);
        @NotNull final Random random = new Random();
        @NotNull final String id = taskList.get(random.nextInt(taskSize)).getId();
        request.setId(id);
        @NotNull final TaskGetByIdResponse response = taskEndpoint.getTaskById(request);
        if (response.getTask() == null) return;
        Assert.assertEquals(id, response.getTask().getId());
    }

    @Test
    @Category(SoapCategory.class)
    public void testGetTaskByIndex() {
        @NotNull final TaskGetByIndexRequest request = new TaskGetByIndexRequest(token);
        @NotNull final TaskListResponse responseList = taskEndpoint.listTask(new TaskListRequest(token));
        @NotNull final Random random = new Random();
        if (responseList.getTasks() == null) return;
        @NotNull final Task task = responseList.getTasks().get(random.nextInt(taskSize));
        final int index = responseList.getTasks().indexOf(task);
        request.setIndex(index);
        @NotNull final TaskGetByIndexResponse response = taskEndpoint.getTaskByIndex(request);
        if (response.getTask() == null) return;
        Assert.assertNotNull(response.getTask());
    }

    @Test
    @Category(SoapCategory.class)
    public void testGetTaskByProjectId() {
        @NotNull final Random random = new Random();
        @NotNull final String projectID = projectList.get(random.nextInt(projectSize)).getId();
        @NotNull final TaskListByProjectIdRequest request = new TaskListByProjectIdRequest(token);
        request.setProjectId(projectID);
        @Nullable final List<Task> tasks = taskEndpoint.getTaskByProjectId(request).getTasks();
        if (tasks == null) return;
        Assert.assertEquals(0, tasks.size());
    }

    @Test
    @Category(SoapCategory.class)
    public void testStartTaskById() {
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(token);
        @NotNull final Random random = new Random();
        @NotNull final String id = taskList.get(random.nextInt(taskSize)).getId();
        @NotNull final String statusValue = "IN_PROGRESS";
        @Nullable final Status status = Status.toStatus(statusValue);
        request.setId(id);
        request.setStatus(status);
        @NotNull final TaskChangeStatusByIdResponse response = taskEndpoint.changeTaskStatusById(request);
        if (response.getTask() == null) return;
        Assert.assertEquals(status, response.getTask().getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void testStartTaskByIndex() {
        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest(token);
        @NotNull final Random random = new Random();
        @NotNull final Task task = taskList.get(random.nextInt(taskSize));
        @NotNull final Integer index = taskList.indexOf(task);
        @NotNull final String statusValue = "IN_PROGRESS";
        @Nullable final Status status = Status.toStatus(statusValue);
        request.setIndex(index);
        request.setStatus(status);
        @NotNull final TaskChangeStatusByIndexResponse response = taskEndpoint.changeTaskStatusByIndex(request);
        if (response.getTask() == null) return;
        Assert.assertEquals(status, response.getTask().getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void testUnbindTaskFromProject() {
        @NotNull final Random random = new Random();
        @NotNull final String projectID = projectList.get(random.nextInt(projectSize)).getId();
        @NotNull final String taskID = taskList.get(random.nextInt(taskSize)).getId();
        @NotNull final TaskBindToProjectRequest requestBind = new TaskBindToProjectRequest(token);
        requestBind.setProjectId(projectID);
        requestBind.setTaskId(taskID);
        taskEndpoint.bindTaskToProject(requestBind);

        @NotNull final TaskUnbindFromProjectRequest requestUnbind = new TaskUnbindFromProjectRequest(token);
        requestUnbind.setProjectId(projectID);
        requestUnbind.setTaskId(taskID);
        taskEndpoint.unbindTaskFromProject(requestUnbind);

        @NotNull final TaskListByProjectIdRequest requestList = new TaskListByProjectIdRequest(token);
        requestList.setProjectId(projectID);
        @Nullable final List<Task> tasks = taskEndpoint.getTaskByProjectId(requestList).getTasks();
        if (tasks == null) {return;}
        Assert.assertEquals(tasks.size(), 0);
    }

    @Test
    @Category(SoapCategory.class)
    public void testUpdateTaskById() {
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(token);
        @NotNull final Random random = new Random();
        @NotNull final String id = taskList.get(random.nextInt(taskSize)).getId();
        @NotNull final String name = "new_name";
        @NotNull final String description = "new description";
        request.setId(id);
        request.setName(name);
        request.setDescription(description);
        @Nullable final Task task = taskEndpoint.updateTaskById(request).getTask();
        if (task == null) {return;}
        Assert.assertEquals(name, task.getName());
        Assert.assertEquals(description, task.getDescription());
    }

    @Test
    @Category(SoapCategory.class)
    public void testUpdateTaskByIndex() {
        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest(token);
        @NotNull final Random random = new Random();
        @NotNull final Task task = taskList.get(random.nextInt(taskSize));
        @NotNull final Integer index = taskList.indexOf(task);
        @NotNull final String name = "new_name";
        @NotNull final String description = "new description";
        request.setIndex(index);
        request.setName(name);
        request.setDescription(description);
        @Nullable final Task taskUpdated = taskEndpoint.updateTaskByIndex(request).getTask();
        if (taskUpdated == null) {return;}
        Assert.assertEquals(name, taskUpdated.getName());
        Assert.assertEquals(description, taskUpdated.getDescription());
    }

}
